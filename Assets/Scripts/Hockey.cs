﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class Hockey : MonoBehaviour {

	public float Force;
	private Vector3 hockeyOrigin;
	private Rigidbody hockey;
	private MeshRenderer meshRenderer;
	private Material originMaterial;
	private Material leftPaddleMaterial;
	private Material rightPaddleMaterial;
	private AudioSource hitSound;

	private MeshRenderer hockeyMeshRenderer;

	private void Start () 
	{
		hockey = GetComponent<Rigidbody>();
		hockeyOrigin = transform.position;
		Launch();

		hitSound = GetComponent<AudioSource>();
		hockeyMeshRenderer = GetComponent<MeshRenderer>();
		originMaterial = hockeyMeshRenderer.material;
		leftPaddleMaterial = GetMaterial(FindObjectOfType<LeftPaddle>()?.gameObject);
		rightPaddleMaterial = GetMaterial(FindObjectOfType<RigthPaddle>()?.gameObject);
	}

	private void Launch()
		=> hockey.AddForce(new Vector3(GetAxis(), GetAxis(), 0f).normalized * Force);

	private float GetAxis()
	{	
		float axis = Random.Range(-.9f, .9f);
		return axis != 0.0f ? axis : GetAxis();
	}

	private void FixedUpdate() 
	{
		float offset = hockey.transform.position.x;
		if(offset > 50 || offset < -50)
			StartCoroutine(Respawn());	
	}


	private Material GetMaterial(GameObject source)
		=> source.GetComponent<MeshRenderer>().material;		

	private void OnCollisionEnter(Collision collision) 
	{	
		ChangeColor(collision);
		if(GameObject.FindObjectOfType<PaddleControl>().gameObject.GetComponent<PaddleControl>().IsPlaying)
			hitSound.Play();
		Vector3 direction = hockey.velocity.normalized;
		hockey.AddForce(Velocity(hockey.velocity.normalized) * Force);
	}

	private void ChangeColor(Collision collision)
	{
		bool leftPaddleHit = collision.gameObject.name == "LeftPaddle";
		bool rightPaddleHit = collision.gameObject.name == "RightPaddle";
		bool goalHit = collision.gameObject.tag == "Goal";

		hockeyMeshRenderer.material = 
			leftPaddleHit ? leftPaddleMaterial : 
				rightPaddleHit ? rightPaddleMaterial : 
					goalHit ? originMaterial : hockeyMeshRenderer.material;
	}

	private Vector3 Velocity(Vector3 velocity)
	{
		velocity.x = velocity.x > 0 ? 1f : -1f;
		velocity.y = velocity.y > 0 ? 
			Random.Range(.25f, 0.65f) : Random.Range(-.25f, -0.65f);

		return velocity;
	}

	private IEnumerator Respawn()
	{
		yield return new WaitForSeconds(1f);
		Spawn();
	}

	public void Spawn()
	{
		transform.position = hockeyOrigin;
		hockey.velocity = Vector3.zero;
		hockey.angularVelocity = Vector3.zero;
		Launch();
	}

}
